﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using temp1.Models;

namespace temp1
{
    public class LogContext: DbContext
    {
        public DbSet<LogModel> LogModels { get; set; }
    }
}