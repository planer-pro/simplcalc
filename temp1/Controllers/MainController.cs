﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using temp1.Models;

namespace temp1.Controllers
{
    public class MainController : Controller
    {
        LogContext db = new LogContext();
        // GET: Main
        public ActionResult Index()
        {
            ViewBag.LogList = db.LogModels.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Index(string action, float number1=0, float number2=0)
        {
            float result = 0;

            switch (action)
            {
                case "+":
                    result = number1 + number2;
                    break;
                case "-":
                    result = number1 - number2;
                    break;
                case "*":
                    result = number1 * number2;
                    break;
                case "/":
                    result = number1 / number2;
                    break;
            }

            LogModel modelToBase = new LogModel();
            modelToBase.result = result;
            modelToBase.dateTime = DateTime.Now;

            db.LogModels.Add(modelToBase);
            db.SaveChanges();

            ViewBag.LogList = db.LogModels.ToList();
            ViewBag.result = result;
            ViewBag.number1 = number1;
            ViewBag.number2 = number2;
            ViewBag.action = action;

            return View();
        }

    }
}